### Component Configuration ###

Send data to and receive narrative from Narrative Science. Please contact your administrator to obtain token for API communication. [Application Introduction](https://docs.google.com/document/d/1EkLA67jxXNifAUtzMvos5ldK40p6CqUX80LkgO-n0Vo).

#### Important parameters
 - Dimensions (specify 1 or 2 column names *present* in the data)
 - Story Type (barchart, linechart, piechart, scatterplot, histogram)
 - Verbosity (low=short narrative, medium, high=long narrative)

#### Final table format
 - timestamp
 - table_name
 - narrative
