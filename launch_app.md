# Create folder for *DATA*:
```
mkdir -p data/{in,out}/{tables,files}
```
_Note_: Store the data folder outside of you repository, since the config.json will contain your secret token...   

# Config file
 1. Use *config_template.json*
 2. Fill the credentials and parameters
 3. rename *config_template.json* to *config.json*
 4. move *config.json* to the *DATA* folder

# Build Image
```
docker build -f Dockerfile -t kbc_narrative_science:latest .
```

# Run from terminal
Alter this script, specify there the location of the *REPOSITORY* and the *DATA* folder:
```
docker run -e "KBC_LOGGER_ADDR=127.0.0.1" -e "KBC_LOGGER_PORT=9401" --volume DATA/kbc_narrative_science/data:/data --volume REPOSITORY/kbc_narrative_science:/code kbc_narrative_science:latest
```

Example:
```
docker run -e "KBC_LOGGER_ADDR=127.0.0.1" -e "KBC_LOGGER_PORT=9401" --volume /Users/martin/Dropbox/Development/kbc_data/kbc_narrative_science/data:/data --volume /Users/martin/Dropbox/Development/kbc_narrative_science:/code kbc_narrative_science:latest
```   
With interactive terminal:
```
docker run -e "KBC_LOGGER_ADDR=127.0.0.1" -e "KBC_LOGGER_PORT=9401" --volume /Users/martin/Dropbox/Development/kbc_data/kbc_narrative_science/data:/data --volume /Users/martin/Dropbox/Development/kbc_narrative_science:/code kbc_narrative_science:latest /bin/bash
```   
