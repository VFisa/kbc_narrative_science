Send data to and receive narrative in human digestible form, powered by Narrative Science. AI-powered automated narrative generation system take raw data, determine what the data mean and then generate easy-to-understand descriptions and explanations in natural language.   
Please contact your administrator to obtain token for API communication. Final narrative is stored within cell of a table, ready for processing.   

#### Supported Features
 - 1-2 Dimensions
 - Story Type (barchart, linechart, piechart, scatterplot, histogram)
 - Verbosity (low=short narrative, medium, high=long narrative)

#### Typical use cases
 - Embedding narrative as an executive summary on the dashboard.
 - Sending narrative daily/weekly/monthly via email (for example via mailgun custom kbc component).
 - Using narrative as a comment field for the data values.
 - Feel free to share yours!
