"__author__ = 'Martin Fiser'"
"__credits__ = 'Keboola 2017, Twitter: @VFisa'"
"__contributors__ = 'Leo from Keboola'"

"""
Python 3 environment (unicode script fixes in place)
"""

import os
import sys
import requests
import json
import logging
import pandas as pd
import time
import csv
from keboola import docker


# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

# Logging
logging.basicConfig(#filename='python_job.log',
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")

# initialize application
cfg = docker.Config('/data/')

# Access the supplied parameters
params = cfg.get_parameters()
token = cfg.get_parameters()["#token"]
index_col = cfg.get_parameters()["index_col"]
verbosity = cfg.get_parameters()["verbosity"]
output_html = cfg.get_parameters()["output_html"]
DEBUG = cfg.get_parameters()["debug"]
STORY = cfg.get_parameters()["story_type"]
parameters = [verbosity, DEBUG, STORY]
# in production, put this into parameter window:
"""
{
    "#token": "string",
    "index_col": ["string"],
    "story_type": "",
    "verbosity": "low",
    "output_html": false,
    "debug": true
}
"""

# if toke is ""
if token == "":
    logging.error("Access deny, response code 403")
    sys.stderr.write("ERROR: 403. Invalid Token Key.")
    sys.exit(0)    

# Get proper list of tables
cfg = docker.Config('/data/')
in_tables = cfg.get_input_tables()
logging.info("IN tables mapped: "+str(in_tables))
out_tables = cfg.get_expected_output_tables()
logging.info("OUT tables mapped: "+str(out_tables))

def verify_dim(parameters):
    """
    Verify all story's dimensions and measures are compatible
    """
    if index_column_count > 2:
        logging.error("{0} dimensioins are not supported. Exit.".format(index_column_count))
        sys.stdout.write("ERROR: More than 2 dimensions are not supported")
        sys.exit(0)
    if parameters == "linechart" or parameters == "barchart":
        if index_column_count == 1:
            logging.info("Dimensions and measures are validated")
        elif (index_column_count == 2) and (columns_count == 1):
            logging.info("Dimensions and measures are validated")
        elif (index_column_count ==2) and (columns_count != 1):
            logging.error("{0} measures are not supported in {1}. Exit.".format(columns_count,parameters))
            sys.stdout.write("ERROR: More than 1 measures are not supported")
            sys.exit(0)
    elif parameters == "piechart" or parameters == "histogram":
        if (index_column_count == 1) and (columns_count == 1):
            logging.info("Dimensions and measures are validated")
        else:
            logging.error("{0} only supports 1 dimension and 1 measure. Exit.".format(parameters))
            sys.stdout.write("ERROR: More than 1 dimensions or measures are not supported. ")
            sys.exit(0)
    elif parameters == "scatterplot":
        if (index_column_count == 1) and (columns_count == 2 or columns_count == 3):
            logging.info("Dimensions and measures are validated")
        elif (index_column_count > 1):
            logging.error("{0} dimensions are not supported in {1}. Exit.".format(index_column_cout,parameters))
            sys.stdout.write("ERROR: More than 1 dimensions are not supported")
            sys.exit(0)
        else:
            logging.error("{0} measures are not supported in {1}. Exit.".format(columns_count,parameters))
            sys.stdout.write("ERROR: More than 3 measures are not supported")
            sys.exit(0)


def verify_params(parameters):
    """
    Verify all parameters whether they comply with API definition
    """

    # story validation
    story_types = ['barchart', 'linechart', 'piechart', 'scatterplot', 'histogram']
    if str(parameters[2]) in story_types:
        logging.info("Story parameter validated.")
        verify_dim(str(parameters[2]))
    else:
        logging.error("Wrong story type selected: "+str(parameters[2])+" Exit.")
        sys.stdout.write("ERROR: Invalid Story type. ")
        sys.exit(1)

    # Verbosity validation
    verbosity_types = ['low', 'medium', 'high']
    if str(parameters[0]) in verbosity_types:
        logging.info("Verbosity parameter validated.")
    else:
        logging.error("Wrong verbosity type selected: "+str(parameters[0])+" Exit.")
        sys.stdout.write("ERROR: Invalid Verbosity type. ")
        sys.exit(1)

    # Debug validation
    if parameters[1] == True:
        logging.info("Debug mode selected.")
    elif parameters[1] == False:
        logging.info("Debug mode NOT selected.")
    else:
        logging.error("Wrong debug option selected: "+str(parameters[1]))

    logging.info("All parameters validated.")
    pass


def get_tables(in_tables, out_tables):
    """
    Evaluate input and output table names.
    Only taking the first one into consideration!
    """

    table = in_tables[0]
    in_name = table["full_path"]
    in_destination = table["destination"]
    logging.info("Data table: " + str(in_name))
    logging.info("Input table source: " + str(in_destination))

    table = out_tables[0]
    out_name = table["full_path"]
    out_Destination = table["source"]
    logging.info("Output table: " + str(out_name))
    logging.info("Output table destination: " + str(out_Destination))

    return in_name, out_name


def input_file(file_in):
    """
    Read input data as CSV DataFrame (pandas)
    """

    try:
        frame = pd.read_csv(file_in, encoding='utf-8')
        logging.info("Input file "+str(file_in)+" read.")
    except Exception as e:
        logging.error('{errormsg}'.format(errormsg = str(e)))

    return frame


def output_file(output_text, file_in):
    """
    Save output data as CSV.
    datetime-table-narrative
    """

    with open(file_out, 'w', encoding='utf-8') as csvfile:
        out_file = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        ## write header
        header = ["timestamp", "table name", "narrative"]
        out_file.writerow(header)

        ## write data
        timestamp = (time.strftime("%Y-%m-%d %H:%M:%S"))
        output_row = [str(timestamp), str(file_in), output_text]
        out_file.writerow(output_row)
        logging.info("Output file produced.")

    csvfile.close()


def output_storage(output_text, file_in):
    """
    Output narrative as HTML file in the file storage
    """

    file = str(str(file_in).split("/")[-1])
    file = str(str(file).split(".")[0])
    html_file = ("/data/out/files/narrative-"+file+".html")
    logging.info("Output HTML file: "+html_file)
    manifest_file = (html_file+".manifest")
    logging.info("Output manifest file: "+manifest_file)

    # File Assembly
    text_body = '<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"></head><body>{0}</body></html>'.format(str(output_text))

    try:
        with open(html_file, 'w', encoding='utf-8') as file_out:
            file_out.write(text_body)
        logging.info("Output HTML file produced.")
    except:
        logging.error("Could not produce output HTML file.")

    # Manifest assembly
    manifest = {
        "is_public": False,
        "is_permanent": True,
        "is_encrypted": True,
        "notify": False,
        "tags": [
            "narrative-science",
            "narrative",
            "html"
        ]
    }

    try:
        with open(manifest_file, 'w', encoding='utf-8') as manifest_out:
            json.dump(manifest, manifest_out)

        logging.info("Output Manifest file produced.")
    except:
        logging.error("Could not produce manifest file.")


def output_request(json_content, file_in):
    """
    Output JSON request file as JSON file in the file storage
    """

    file = str(str(file_in).split("/")[-1])
    file = str(str(file).split(".")[0])
    json_file = ("/data/out/files/narrative-api-request-"+file+".json")
    logging.info("Request JSON file: "+json_file)
    manifest_file = (json_file+".manifest")
    logging.info("Request JSON manifest file: "+manifest_file)

    try:
        with open(json_file, 'w', encoding='utf-8') as file_out:
            json.dump(json_content, file_out)
        logging.info("Request JSON file produced.")
    except:
        logging.error("Could not produce Request JSON file.")

    # Manifest assembly
    manifest = {
        "is_public": False,
        "is_permanent": True,
        "is_encrypted": True,
        "notify": False,
        "tags": [
            "narrative-science",
            "narrative",
            "json"
        ]
    }

    try:
        with open(manifest_file, 'w', encoding='utf-8') as manifest_out:
            json.dump(manifest, manifest_out)

        logging.info("Request JSON Manifest file produced.")
    except:
        logging.error("Could not produce Request JSON manifest file.")


def load_json(file_in):
    """
    Read input data as JSON
    """

    #with open(file_in, 'r', encoding='utf-8') as jsonfile: # Python3
    with open(file_in, 'r') as jsonfile:
        try:
            payload = json.load(jsonfile)
            logging.info("Template JSON loaded")
        except:
            logging.error("Could not load json template file. Exit.")
            sys.stdout.write("ERROR: Could not load json template file. ")
            sys.exit(1)

    logging.info(str(file_in)+" file read.")
    jsonfile.close()

    return payload


def save_json(data, file_out):
    """
    Save JSON data as JSON file
    """

    filename = str(file_out)+".json"

    with open(filename, 'w') as jsonfile:
        try:
            json.dump(data, jsonfile)
        except:
            logging.error("Could not output json file.")

    logging.info(str(file_out)+" file saved.")
    jsonfile.close()


def send_request(token, payload, STORY):
    """
    Send POST to Narrative Science API.
    Data and details in the JSON encoded payload.
    """

    base_url = ("https://viz.narrativescience.com/v2/stories/"+str(STORY))

    headers = {
        'Content-Type': 'application/json'
        }

    params = {
        "user_key": token,
        "detailed": "true"
    }

    response = requests.post(base_url, headers=headers, params=params, json=payload)

    if DEBUG == True:
        print(response.status_code)
        print(response.headers)

    if response.status_code == 400:
        logging.error("Bad request, response code 400.")
        sys.stdout.write("ERROR: 400. Bad Request. ")
        sys.exit(1)
    elif response.status_code == 404:
        logging.error("Bad authentication, response code 404.")
        sys.stdout.write("ERROR: 404. Bad authentication. ")
        sys.exit(1)
    elif response.status_code == 500:
        logging.info("Server error, response code: 500.")
        sys.stdout.write("ERROR: 500. Server error. ")
        sys.exit(0)
    elif response.status_code == 403:
        logging.error("Access deny, response code 403")
        sys.stderr.write("ERROR: 403. Invalid Token Key.")
        sys.exit(0)
    elif response.status_code == 200:
        logging.info("Received correct response code: 200.")
        received_text = response.text
    else:
        logging.info("API request response: "+str(response.status_code))
        received_text = ""

    logging.debug(received_text)

    return received_text


def get_count():
    max_value = max(model[his_column])+1
    min_value = min(model[his_column])-1
    steps_value = (max_value-min_value)/steps
    temp_value = min_value
    order = []     
    range_values = [int(min_value)]
    n = 0
    for a in index_column:
        while n<steps:
            order.append(str(int(temp_value))+" <= "+str(a)+" < "+str(int(temp_value+steps_value)))
            temp_value += steps_value
            range_values.append(int(temp_value))
            n += 1
    #order = json.dumps(order)
    i = 0
    j = 0
    while i < (steps):
        count_irr = 0
        while j < len(model[his_column]):
            if model[his_column][j]>=range_values[i] and model[his_column][j]<range_values[i+1]:
                count_irr += 1
            j+=1
        i+=1
        j=0
        count_value.append(count_irr)

    return order


def get_dimensions():
    """
    Define list of dimensions based on parameters
    """
    dimensions = []
    if STORY != "histogram":
        ### Dimension 1 ###
        if index_column_count == 1:
            for a in index_column:
                dim_piece = {
                        "name": str(a),
                        "id": str(a).lower()[:10]
                    }
                dimensions.append(dim_piece)
        ### Dimension 2 ###
        else:
            for a in index_column:
                value = str(a).replace(" ","")
                dim_piece = {
                        "name": value,
                        "id": value.lower()[:10]
                }
                dimensions.append(dim_piece)                
    if STORY == "histogram":
        for a in index_column:
            dim_piece = {
                    "name": (str(a)+" Range"),    
                    "id": str(a).lower()[:10],
                    "order": ""
                }
            dimensions.append(dim_piece)            

    if DEBUG == True:
        print("Dimensions: "+str(dimensions))

    return dimensions


def get_measures():
    """
    Define measures from the list of columns
    Use columns not used for dimensions
    """

    measures = []
    for a in columns:
        piece = str(a).replace(" ","")
        if STORY == "histogram":
            measure_piece = {
                    "name": (piece+" Count"),
                    "id": piece.lower()[:10]
                }
            measures.append(measure_piece)
        else:
            measure_piece = {
                    "name": piece,
                    "id": piece.lower()[:10]
                }
            measures.append(measure_piece)

    if DEBUG == True:
        print("Measures: "+str(measures))

    return measures


def get_rows():
    """
    Assemble data as a rows section of JSON payload
    """

    # rows
    rows = []
    id_num = 1
    for index, row in model.iterrows():
        row_point = []


        # multi-columns fix attempt
        for c in index_column:
            header_value = row[c]
            header = {
                "value": header_value,
                "id": id_num
            }
            id_num +=1
            row_point.append(header)
        """
        
        header_value = row[index_column[0]]
        header = {
            "value": header_value,
            "id": id_num
        }
        id_num +=1
        row_point.append(header)

        """

        for b in columns:
            data_value = row[b]
            data_point = {
                "value": data_value,
                "id": id_num
            }
            id_num +=1
            row_point.append(data_point)
        rows.append(row_point)

    return rows


def get_rows_histo():
    """
    Assemble data as a rows section of JSON payload for histogram
    """
    # rows
    rows = []
    id_num = 1
    i = 0
    while i < steps:
        row_point = []
        #output multi columns with header and values
        header_value = count_list[i]
        header = {
            "value": header_value,
            "id": id_num
        }
        id_num += 1
        row_point.append(header)
        data_value = count_value[i]
        data_point = {
            "value": data_value,
            "id": id_num
        }
        id_num += 1
        row_point.append(data_point)
        rows.append(row_point)
        i+=1

    return rows


def produce_payload():
    """
    Asseble the final JSON payload.
    """

    # load template payload
    if STORY == "histogram":
        payload_template = load_json("payload_template_histogram.json")
    else:
        payload_template = load_json("payload_template.json")

    # change json payload with data
    payload = payload_template
    payload["data"]["dimensions"] = dimensions
    if STORY == "histogram":
        payload["data"]["dimensions"][0]["order"] = count_list
    payload["data"]["measures"] = measures
    payload["data"]["rows"] = rows
    payload["configuration"]["authoring"]["verbosity"] = verbosity
    # more options possible here, lets say configuration sections

    return payload


if __name__ == "__main__":
    """
    """

    # Debug mode info
    if DEBUG == True:
        logging.info("Debug mode selected.")

    # Determine data destination
    file_in, file_out = get_tables(in_tables, out_tables)
    logging.debug("Mapped files. \n IN: {0} \n OUT: {1}".format(file_in, file_out))

    # Load data into pandas DF
    model = input_file(file_in)

    # Validate parameters
    index_column_count = len(index_col)
    columns_count = len(model.columns.values) - index_column_count
    verify_params(parameters)

    # Get list of columns
    model_columns = list(model.columns.values)
    logging.debug("File columns: {0}".format(model_columns))

    # Columns
    index_column = index_col
    temp = 0
    columns = model_columns
    while temp < index_column_count:
        n = columns.index(index_column[temp])
        del columns[n]
        temp+=1
    his_column = index_col[0]
    steps = 5
    columns = set(columns)
    logging.debug("Data columns: {0}".format(columns))


    # Assemble model pieces
    dimensions = get_dimensions()
    measures = get_measures()

    count_value = []
    count_list = []
    if STORY == "histogram":
        count_list = get_count()
        rows = get_rows_histo()
    else:
        rows = get_rows()

    # Assemble payload
    payload = produce_payload()

    # Output api request as json file as well
    if DEBUG == True:
        print(payload)
        output_request(payload, file_in)
    else:
        logging.info("API Request JSON file not requested.")

    # Call API
    text = send_request(token, payload, STORY)
    logging.debug("Received text: {0}".format(text))
    if DEBUG == True:
        print("Received text: {0}".format(text))

    # Save output into table
    output_file(text, file_in)

    # Output narrative as html file as well
    if output_html == True:
        output_storage(text, file_in)
    else:
        logging.info("Output HTML file not requested.")

    logging.info("Script finished.")
